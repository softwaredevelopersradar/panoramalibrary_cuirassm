﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitPLibraryEvents();

            pLibrary.ViewMode = 1;

            pLibrary.GlobalRangeXmin = 10;
            pLibrary.GlobalBandWidthMHz = 120;
            pLibrary.GlobalNumberOfBands = 50;
            pLibrary.Threshold = 50;
        }

        

        public void InitPLibraryEvents()
        {
            //pLibrary.NeedSpectrumRequest += PLibrary_NeedSpectrumRequest;

            pLibrary.ThresholdChange += PLibrary_ThresholdChangeAsync;
        }
       

        private object lockObject = new object();

        private async void PLibrary_ThresholdChangeAsync(object sender, int value)
        {
            //отправить на сервер
            //var answer00 = await dsp.SetFilters(value, 0, 0, 0);
            //или передать куда ещё, а потом на сервер
        }

        //1
        private void one_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 1;
        }
        //2
        private void two_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 2;
        }
        //3
        private void two_plus_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 3;
        }
        //4
        private void three_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 4;
        }
        //5
        private void four_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 5;
        }
        //Connect/Disconnect
        private async void five_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush brush = (SolidColorBrush)five.Background;
            if (brush.Color == Colors.Red)
            {
                var Address = "192.168.1.102";
                //var Address = "192.168.0.193";
                //var Address = "192.168.0.111";
                var Port = 10001;
                //подключение к серверу
               // await dsp.ConnectToBearingDSP(Address, Port);

                five.Background = new SolidColorBrush(Colors.Green);
            }
            else
            {
                //отключение от сервера
                //dsp.DisconnectFromBearingDSP();
                five.Background = new SolidColorBrush(Colors.Red);
            }
        }

        //Подготовка
        private async void six_Click(object sender, RoutedEventArgs e)
        {
            //отправка режима работы 0 на сервер
            //var answer = await dsp.SetMode(0);
            pLibrary.Mode = 0;

        }

        //РР 1
        private async void seven0_Click(object sender, RoutedEventArgs e)
        {
            //отправка режима работы 1 на сервер
            //var answer = await dsp.SetMode(1);

            pLibrary.Mode = 1;
        }


        bool spectrum = false;
        private async void Spectrum_Click(object sender, RoutedEventArgs e)
        {
            spectrum = !spectrum;
            if (spectrum)
            {
                pLibrary.Mode = 1;

                pLibrary.СhooseFPS(PanoramaLibrary.PLibrary.FramesPerSecond.FPS30);
                //запрос спектра от сервера
                //var answer0 = await dsp.SetSectorsAndRanges(0, 0, 25.0d, 100.0d, 0, 360); //Шифр 3
            }
            else
            {
                pLibrary.Mode = 0;
            }
        }
       
        private async void PLibrary_NeedSpectrumRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            //запрос спектра от сервера
            //var answer = await dsp.GetSpectrum(MinVisibleX, MaxVisibleX, PointCount);
            //if (answer?.Header.ErrorCode == 0)
            {
                //if (answer.Spectrum != null)
                {
                    //отрисовка спектра
                    //pLibrary.IQSpectrumPainted(MinVisibleX, MaxVisibleX, answer.Spectrum);
                }
            }
        }

        int MinSpectrValue = -130;

        private void none_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ClearStorage();
        }

        private void one_Click2(object sender, RoutedEventArgs e)
        {
            pLibrary.GlobalNumberOfBands = 200;
        }

        private void two_Click2(object sender, RoutedEventArgs e)
        {
            pLibrary.GlobalRangeXmin = 100;
        }

        private void three_Click2(object sender, RoutedEventArgs e)
        {
            double[] d = { 25d,35d,55d,65d,3000d,3025d };
            pLibrary.ConvertValueToIndexes(d);

        }

        private void four_Click2(object sender, RoutedEventArgs e)
        {
            //pLibrary.Threshold = 100;

            //pLibrary.ExternalExBearing(50,10);

            pLibrary.SetLanguage("eng");
        }

        private async void five_Click2(object sender, RoutedEventArgs e)
        {

            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 25, 3000 }, new double[] { 55, 3025 });
            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 30, 300 }, new double[] { 40, 400 });

            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 100, 1000 }, new double[] { 500, 2000 });


            //var answer = await dsp.SetMode(2);
            pLibrary.Mode = 2;


        }

        private async void six_Click2(object sender, RoutedEventArgs e)
        {
            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRS, new double[] { 500, 2000 }, new double[] { 1000, 3000 });
            //var answer = await dsp.SetMode(0);
            pLibrary.Mode = 0;
        }

        private async void seven_Click2(object sender, RoutedEventArgs e)
        {
            //byte[] Bands = new byte[100];
            //for (int i = 0; i < Bands.Count(); i++)
            //{
            //    Bands[i] = (byte)i;
            //}
            //var answer = await dsp.GetBandsAdaptiveThreshold(Bands);


            List<double> lFreqStart = new List<double>();
            List<double> lFreqEnd = new List<double>();
            List<double[]> lCutOffFreq = new List<double[]>();
            List<double[]> lCutOffWidth = new List<double[]>();
            List<double> lThreshold = new List<double>();

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(30);
                lFreqEnd.Add(50);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(70);
                lFreqEnd.Add(100);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            pLibrary.FHSSonRS(lFreqStart, lFreqEnd, lCutOffFreq, lCutOffWidth, lThreshold);

            //var answer = await dsp.SetMode(6);
            pLibrary.Mode = 6;

        }

        private void Spectrum2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {

        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
