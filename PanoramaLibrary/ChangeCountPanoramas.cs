﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanoramaLibrary
{
    public partial class PLibrary
    {   
        public enum CorrelationVisibility
        {
            True,
            False
        }

        public delegate void SendComboBoxIndex(int count);
        public event SendComboBoxIndex OnSendCBIndex = (count) => { };
        public event SendComboBoxIndex OnSendCBCorrIndex = (count) => { };

        public delegate void CorrPanoramaVisibility(CorrelationVisibility correlationVisibility);
        public event CorrPanoramaVisibility OnCorrelationVisibility = (visibility) => { };

        private int countPanoramas = 5;
        public int CountPanoramas
        {
            get => countPanoramas;
            set
            {
                countPanoramas = value;
                spControl.Index = countPanoramas;
            }
        }

        private int countCorrPanoramas = 3;

        public int CountCorrPanoramas 
        {
            get => countCorrPanoramas;
            set 
            {
                countCorrPanoramas = value;
                spControl.IndexCorr = countCorrPanoramas;
            }
        }

        private void SpControl_OnSendCBIndex(int count)
        {
            OnSendCBIndex?.Invoke(count);
        }

        private void SpControl_OnSendCBCorrIndex(int count)
        {
            OnSendCBCorrIndex?.Invoke(count);
        }

        private void SpControl_OnCorrelationVisibility(SpectrumPanoramaControl.SPControl.CorrelationVisibility correlationVisibility)
        {
            switch (correlationVisibility)
            {
                case SpectrumPanoramaControl.SPControl.CorrelationVisibility.True:
                    OnCorrelationVisibility?.Invoke(CorrelationVisibility.True);
                    break;
                case SpectrumPanoramaControl.SPControl.CorrelationVisibility.False:
                    OnCorrelationVisibility?.Invoke(CorrelationVisibility.False);
                    break;
                default:
                    break;
            }            
        }

        public void CorrPanoramaIsVisibility(CorrelationVisibility correlationVisibility)
        {
            switch (correlationVisibility)
            {
                case CorrelationVisibility.True:
                    spControl.CorrPanoramaIsVisibility(SpectrumPanoramaControl.SPControl.CorrelationVisibility.True);
                    break;
                case CorrelationVisibility.False:
                    spControl.CorrPanoramaIsVisibility(SpectrumPanoramaControl.SPControl.CorrelationVisibility.False);
                    break;
                default:
                    break;
            }               
        }
    }
}
