﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanoramaLibrary
{
    public class ChannelsStatusEventArgs : EventArgs
    {
        public ChannelsStatusEventArgs(bool channel1, bool channel2, bool channel3, bool channel4, bool mx, bool md, bool sm)
        {
            Channel1 = channel1;
            Channel2 = channel2;
            Channel3 = channel3;
            Channel4 = channel4;
            Mx = mx;
            Md = md;
            Sm = sm;
        }

        public bool Channel1 { get; set; }
        public bool Channel2 { get; set; }
        public bool Channel3 { get; set; }
        public bool Channel4 { get; set; }
        public bool Mx { get; set; }
        public bool Md { get; set; }
        public bool Sm { get; set; }
    }
}
