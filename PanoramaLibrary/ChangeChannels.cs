﻿using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PanoramaLibrary
{
    public partial class PLibrary : UserControl
    {
        public event EventHandler<ChannelsStatusEventArgs> OnChannelStatus;

        List<bool> statusChannels = new List<bool> { false, false, false, false, false, false, false };
        private void SpControl_OnSendChannelStatus(object sender, byte id, bool status)
        {
            statusChannels[id - 1] = status;
            OnChannelStatus?.Invoke(this, new ChannelsStatusEventArgs(statusChannels[0], statusChannels[1], statusChannels[2], statusChannels[3], statusChannels[4], statusChannels[5], statusChannels[6]));
        }

        public void UpdateChannels(bool channel1, bool channel2, bool channel3, bool channel4, bool channelSum, bool channelMedian) 
        {
            statusChannels = new List<bool> { channel1, channel2, channel3, channel4, channelSum, channelMedian };
        }

        public void DefaultChannels(bool channel1, bool channel2, bool channel3, bool channel4)
        {
            spControl.DefaultChannels(channel1, channel2, channel3, channel4);
        }
    }
}
